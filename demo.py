class Solution:
    def mapItOut(self, flights):
        dict = {}
        output = []
        for key, val in flights:
            dict.setdefault(key, []).append(val)

        element = "DEL"
        output.append(element)

        for item in output:
            if item in dict:
                elements = dict.get(item)
                elements.sort()
                for e in elements:
                    output.append(e)

        return output

        # write your solution here.


def main():
    flights = []
    while(True):
        try:
            routes = [i for i in input().strip().split()]
            flights.append(routes)
        except EOFError:
            break

    output = Solution().mapItOut(flights)
    for i in output:
        print(i, end=" ")


if __name__ == '__main__':
    main()
