"""
Decorators :
Any callable python object (eg. function) taht is used to modify any function or a class.
- Function Decorators
- Class Decorators

Criteria to create Decorators:
1 Need to take functionas a parameter.
2 Add fuctionality/logic to the function.
3 function needs to return the another function.

"""


def div(a, b):
    print(a/b)


def deco(func):
    def inner(a, b):
        if (a < b):
            a, b = b, a
        return func(a, b)
    return inner


div1 = deco(div)

div1(2, 4)
