class Computer:
    # It will be called by default , when the object is initiated
    def __init__(self, cpu, ram):
        self.cpu = cpu
        self.ram = ram
        self.name = "Default"
        self.age = 10
        print("In init")

    def config(self):
        print('Config : ', self.cpu, self.ram, self.name, self.age)

    def update(self):
        self.age = 15

    def compare(self, other):
        if self.age == other.age:
            return True
        else:
            return False


com1 = Computer('i5', 16)  # constructor, which calls init method internally
com2 = Computer('i7', 32)
com1.name = 'Lenovo'
com1.update()
print(type(com1))

Computer.config(com1)
Computer.config(com2)

com1.config()
com2.config()

if com1.compare(com2):
    print('They are same Computers')
else:
    print('Ther are diffrent Computers')

print("-------------------------------------------------------------")
