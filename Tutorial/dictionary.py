dictionary = {'name': 'Kashish', 'age': '24', 'mobile': '8290784910'}
print(dictionary)
print(dictionary['name'])


fetch = dictionary.get('age')  # 24 #Fetched the index
print(fetch)
# get returns NONE, if no index is there.
# Instead of none, we can print any tex - eg Not Found
print(dictionary.get('hello', 'Not Found'))


# Convert List into Dictionary
keys = ['name', 'age', 'gender']
values = ['Kashish', '24', 'Female']
data = dict(zip(keys, values))
print(data)  # {'name': 'Kashish', 'age': '24', 'gender': 'Female'}


# add data in dictionary
data['number'] = '8290784910'
print(data)


# delete from dictionary
del data['gender']
print(data)

# dictionary of Dictionaries, List, etc...
dataphi_projects = {'moodle': 'PHP',
                    'Assure': ['Python', 'ERPNext', 'Frappe'],
                    'Popmart': {'APIS': 'PHP',
                                'WEB': ['PHP', 'opencart'],
                                'MERCHANT': ['Python', 'Django']
                                }
                    }
print(dataphi_projects)
print(dataphi_projects['Popmart']['WEB'][0])  # PHP
print(dataphi_projects['Assure'][1])  # ERPNext
