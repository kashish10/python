class Student:

    def __init__(self, name, rollno):
        self.name = name
        self.rollno = rollno

    def show(self):
        print(self.name, self.rollno)

    # Inner Calss

    class Laptop:

        def __init__(self, brand, ram, storage):
            self.brand = brand
            self.ram = ram
            self.storage = storage

        def show(self):
            print(self.brand, self.ram, self.storage)


s1 = Student('Kashish', 21)
lap1 = Student.Laptop('Lenovo', 'i5', 32)
lap1.show()
s1.show()
