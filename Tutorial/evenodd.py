def evenodd(list):
    even = 0
    odd = 0
    for e in list:
        if e % 2 == 0:
            even += 1
        else:
            odd += 1

    return even, odd


result_even, result_odd = evenodd([1, 2, 3, 4, 5, 6, 7, 8, 9])
print('Even : {}, Odd : {}'.format(result_even, result_odd))
