from array import *
import math


vals = array('i', [5, 9, 8, 4, 2])
# buffer_info ()(2577986161584, 5) gives address of array, size of array
print(vals.buffer_info())
vals.reverse()
print("From Old Array")
for e in vals:
    print(e)

# copy to an new array
new_array = array(vals.typecode, (a for a in vals))
i = 0
print("From New Array")
while(i < len(new_array)):
    print(new_array[i])
    i = i+1

# Sort Array in ascending order :
print('SORTING')
unsortedArray = array('i', [8, 7, 4, 2, 5, 1])

for e in range(0, len(unsortedArray)):
    for j in range(e+1, len(unsortedArray)):
        temp = 0
        if(unsortedArray[e] > unsortedArray[j]):
            temp = unsortedArray[e]
            unsortedArray[e] = unsortedArray[j]
            unsortedArray[j] = temp


print(unsortedArray)

unsortedArray2 = array('i', [8, 7, 4, 2, 5, 1])
print("Sorted array 2 : using function :", sorted(unsortedArray2))

# to find the factorial of the given number :

n = int(input('Enter any numberto get the factorial of it'))
print(math.factorial(n))

print('Using Logic')
fact = 1
for i in range(1, n+1):
    fact = i * fact
print(fact)

# Enter values from user and perform search operation

arr = array('i', [])

size = int(input('Enter the no. of values u want in array'))

for i in range(0, size):
    val = int(input('Enter the values'))
    arr.append(val)
for e in arr:
    print(e)

search = int(input('Enter the no. u want to search in array : '))
for i in range(len(arr)):
    if (arr[i] == search):
        print("Index is : ", i)
        break
else:
    print('No. doesnt exist in array !')
print("Function used !", arr.index(search))

#  Create an array with 5 values and delete the value at index number 2 without using in-built functions.

arr1 = array('i', [2, 3, 5, 1, 7])
arr1.pop(2)
print(arr1)


#  Write a code to reverse an array without using in-built functions.

arr = array('i', [2, 3, 5, 1, 7])

for i in range(len(arr)-1, -1, -1):
    print(arr[i])
    arr.append(arr[i])
    arr.pop(i)
print(arr)
