from functools import reduce


def is_even(n):
    return n % 2 == 0


def double(n):
    return n*2


def add(a, b):
    return a+b


nums = [1, 2, 3, 4, 5, 6, 7, 8, 9]

evens = list(filter(is_even, nums))
evens_lambda = list(filter(lambda n: n % 2 == 0, nums))

doubles = list(map(double, evens))
doubles_lambda = list(map(lambda n: n * 2, evens))

adds = reduce(add, doubles)
adds_lambda = reduce(lambda a, b: a + b, doubles_lambda)

print(evens)
print(evens_lambda)
print(doubles)
print(doubles_lambda)
print(adds)
print(adds_lambda)
