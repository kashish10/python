from numpy import arange, array, cos, info, log, matrix, sin, zeros
from numpy.core import arrayprint
from numpy.core.fromnumeric import diagonal, partition
from numpy.core.function_base import linspace, logspace
from numpy.core.numeric import ones
from numpy.ma.core import concatenate
# from array import *

arr = array([1, 2, 3, 4, 5, 6, 7, 8, 9])
print(arr)

# Ways of creating Arrays :
# array()
arr = array([1, 2, 3, 4, 5, 6, 7, 8, 9])
print(arr)

# linspace()
# (start, stop(inclusive), parts), if parts not entered, by default 50 is taken.
arr = linspace(0, 20, 5)
print(arr)

# logspace()
# difference between elements depends of logaritmice value of 5
arr = logspace(1, 20, 5)
print(arr)

# arange()
arr = arange(0, 10, 2)  # 2 skips
print(arr)

# zeros()
arr = zeros(5, int)
print(arr)

# ones()
arr = ones(5, int)
print(arr)


# Adding 5 to each elements :
arr1 = array([1, 2, 3, 4, 5])
arr1 = arr1 + 5
print(arr1)
# adding 2 arrays (Vectorized Operation)
arr1 = arr1 + arr
print(arr1)

# Few inbuilt functions :
print(min(arr1))
print(max(arr1))
print(sin(arr1))
print(cos(arr1))
print(log(arr1))
print(concatenate([arr1, arr]))

"""
Copying an array
2 types of copy : shallow and deep
shallow, dependent on other. Its linked.
deep copy : independent of other arrays

"""
# aliasing
arr_copy = arr
print(arr_copy)
# both will be pointing to the same memory address, it will not be called as copy. :)
print(id(arr))
print(id(arr_copy))

# Now address will be different
# shallo copy - view()
arr_copy = arr.view()
print(arr_copy)
print(id(arr))
print(id(arr_copy))
arr[0] = 10
print(arr_copy)


# Now address will be different
# deep copy - view()
arr = array([1, 2, 3, 4, 5])

arr_copy_deep = arr.copy()
print(arr_copy_deep)
print(id(arr))
print(id(arr_copy_deep))
arr[0] = 20
print(arr)
print(arr_copy_deep)

# Code to write 2 arrays using for loop

arr1 = array([1, 2, 3, 4, 5])
arr2 = array([1, 1, 1, 1, 1])
sum_arr = array([0, 0, 0, 0, 0])

for i in range(0, len(arr1)):
    add = arr1[i] + arr2[i]
    sum_arr[i] = add
print(sum_arr)

# Maximum value in an array
arr1 = array([1, 6, 2, 4, 8, 3])
max = 0
for e in arr1:
    if(e > max):
        max = e
print(max)

# Multi Dimensional Array :
mat1 = array([[1, 2, 3, 7, 8, 9],
              [4, 5, 6, 10, 11, 12]
              ])

print(mat1)
print(mat1.dtype)
print(mat1.ndim)
print(mat1.shape)
print(mat1.size)
mat2 = mat1.flatten()
print(mat2)
mat3 = mat1.reshape(3, 4)
print(mat3)
mat3_2 = mat1.reshape(2, 2, 3)
print(mat3_2)

array1 = array([[1, 2, 3],
                [5, 6, 7],
                [3, 7, 8]
                ])

matrix1 = array(array1)
print("Matrix : ", matrix1)
matrix2 = matrix('1 2 3 ;4 5 6 ;7 8 9')
print(matrix2)
print("Diagonal Matrix", diagonal(matrix2))
print(matrix2.min())
print(matrix1 * matrix2)
