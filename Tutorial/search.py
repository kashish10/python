# -------------------------------------------------LINEAR SEARCH----------------------------------------------------------
"""
A linear search scans one item at a time, without jumping to any item .

The worst case complexity is  O(n), sometimes known an O(n) search
Time taken to search elements keep increasing as the number of elements are increased.

"""
# arrr = [5, 8, 4, 6, 9, 2]
# Search = 9
# By Linear Search
pos = -1

# While Loop


def linSearch(list, s):
    i = 0
    while(i < len(list)):
        if(list[i] == s):
            globals()['pos'] = i
            return i
        i = i+1
    else:
        return False


arr = [5, 8, 4, 6, 9, 2]
s = 9
if linSearch(arr, s):
    print("Linear Seach --- Exists at ", pos)
else:
    print("Linear Seach ---Not Exists")

# For Loop :
for i in range(len(arr)):
    if(arr[i] == s):
        print("Linear Seach --- The element ", s,
              " is in the list at position ", i)
        break
else:
    print("Linear Seach --- The element ", s, " is not in the list.")

# -------------------------------------------------BINARY SEARCH----------------------------------------------------------
"""
Values must be sorted in Binary Search.

A binary search however, cut down your search to half as soon as you find middle of a sorted list.

The middle element is looked to check if it is greater than or less than the value to be searched.
Accordingly, search is done to either half of the given list

O(log n)

"""

list = [4, 7, 8, 12, 45, 99]
s = 45

position = -1


def binSearch(list, s):
    l = 0
    u = len(list) - 1
    for i in range(len(list)):
        m = (l + u)//2
        if list[m] == s:
            globals()['position'] = m
            return True
        else:
            if s > list[m]:
                l = m
            else:
                u = m
    else:
        return False


if binSearch(list, s):
    print("Binary Seach ---Exists at position :", position)
else:
    print("Binary Search ---Not Exists")
