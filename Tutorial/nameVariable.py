from nameVariable_ref import greet


def main():
    print('Hello from , nameVariable File')
    print('Welcome User')
    print('Name variable from main fun : ', __name__)
    greet()


# main()
if(__name__ == "__main__"):
    main()
