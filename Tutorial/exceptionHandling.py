a = 7
b = 0

try:
    print("Open resource")
    print(a/b)
    k = int(input('enter the value'))
    print(k)
except ZeroDivisionError as e:
    print("You cant divide a number by 0", e)
except ValueError as e:
    print('input is wrong')
except Exception as e:
    print('Something Went Wrong!')
finally:  # finally will execute , after getting error as well as not getting error. It will always execute unless u kill/system exit :)
    print("Close resource")
print("Kashish")
