f = open('PythonNotes.txt', 'r')  # Opens the file in read mode
# print(f.read())  # Read the file
print(f.readline(), end="")  # Read the First line of file
print(f.readline())  # Read the 2nd line of file
print(f.readline())  # Read the 3rd line of file
print(f.readline())  # Read the 4th line of file
print(f.readline(4))  # Read the 5th line of file - 4 characters


f1 = open('PythonNotes.txt', 'a')  # Opens the file in append mode
f1.write('Writing in file from filHandling.py code ! :) Yaye!')

f2 = open('file_by_created_by_code.txt', 'w')
# To copy data from one file to another

for data in f:
    f2.write(data)

# rb means - read binary . Eg. -to read image file
# f2 = open('abc.jpeg', 'rb')
