
def greet():
    print('Hello from, nameVariable_ref File greet 1')
    print("Name variable from greet 1 fun : ", __name__)


def greet2():
    print('Hello from, nameVariable_ref File greet 2')
    print("Name variable from greet 2 fun : ", __name__)


def main():
    print('Hello from, nameVariable_ref File MAIN')
    greet()
    greet2()


if (__name__ == "__main__"):
    main()
