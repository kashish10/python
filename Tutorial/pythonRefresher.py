name = 'Kashish'
age = 24
print(f"Hello, I am {name} and my age is {age}")
print("Hello, I am {} and my age is {}".format(name, age))

# Lists
names = ['kashish', 'mark', 'sallen', 'nick', 2.4]
print(len(names))
names.insert(1, "curie")
names.append('zack')
del(names[3])
names[3] = "Alia Bhatt"
print(names)

# Dictionaries
names = {'kashish': 24, "jack": 30, "hellen": 10}
print(names)
print(names['kashish'])
del(names["hellen"])
names['sally'] = 56
names['jack'] = 56
print(names)

# Classes


class Name:

    def __init__(self, name ="", age=0):
        self.name = name
        self.age = age

    Name = "This is from name class"
    # methods

    def name(self, name):
        print("kashish" + " " + name)

    def age(self, age=0):
        print(age)


# myname = Name()
# myname.name()
# myname.age(24)
# myname.course = "Python"
# print(myname.course)
# print(Name.Name)

mynameCurie = Name("Curie", 25)
mynameCurie2 = Name()
