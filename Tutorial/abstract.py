from abc import ABC, abstractmethod


class A(ABC):

    @abstractmethod
    def method(self):
        pass


class B(A):
    def method(self):
        print('Override :)')


# a1 = A()  # It will give error. You cannot create the object of Abstract class.
b1 = B()
b1.method()
