from typing import DefaultDict


def factorial(n):
    if n >= 0:
        if n == 0:
            print("1")
        else:
            factorial = 1
            for i in range(n, 1, -1):
                factorial = factorial * i
            print(factorial)


factorial(5)

# ------------------------------------ RECUSRSION


def fact(n):
    if n == 0:
        return 1
    return n * fact(n-1)


fact(5)
