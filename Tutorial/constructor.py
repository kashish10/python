
"""
If u create a subclass/child class, it will first try to find the init of child class, if not found then it iwll call of super class/
parent class

when u have super() , then it wll call parent class first and then child class.
super() - We can call method as well as init method.

"""


class A:
    def __init__(self):
        print("In A init")

    def feature1(self):
        print('Feature1 - A')


class B():
    def __init__(self):
        print("In B init")

    def feature2(self):
        print('Feature2')

    def feature1(self):
        print('Feature1 - B')


class C(B, A):  # MRO - Method Resolution Order - left to right. Hece perefrence given to B
    def __init__(self):
        super().__init__()
        print("In C init")

    def feature3(self):
        print('Feature3')

    def feat(self):
        super().feature2()


# a1 = A()
# b1 = B()
c1 = C()
c1.feature1()  # In feature also, MRO Concept
c1.feat()
