# Using extra variable :
# 3 Bits required
a = 10
b = 20

print("Before Swap : ")
print(a)
print(b)
temp = a
a = b
b = temp

print("After Swap : ")
print(a)
print(b)

# No Extra Variable :
# 4 bits , Hence bit wastage
a = a + b
b = a - b
a = a - b

print("After 2nd Swap : ")
print(a)
print(b)

# Hence, to resolve bits issue, we use XOR
a = a ^ b
b = a ^ b
a = a ^ b

print("After 3rd Swap : ")
print(a)
print(b)

# Only works In Python. Stack. ROT_TWO() function is used in behind.-> Swaps two top most stack items.
a, b = b, a

print("After 4th Swap : ")
print(a)
print(b)
