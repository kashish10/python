# DUCK TYPING

class Vscode:
    def execute(self):
        print('Compiles')
        print('Runs')


class MyEditor:
    def execute(self):
        print('Spell Check')
        print('Shows Documentations')
        print('Compiles')
        print('Runs')


class Laptop:
    def code(self, ide):
        ide.execute()


# ide = Vscode()
ide = MyEditor()

lap1 = Laptop()
lap1.code(ide)

# -----------------------------------------------------------------------------------
# OPERATOR OVERLOADING

a = 5
b = 6

c = '5'
d = '6'

# the moment we use add operator +, it calls __add__()
print(a+b)
print(int.__add__(a, b))
print(c+d)
print(str.__add__(c, d))


class Student:
    def __init__(self, m1, m2):
        self.m1 = m1
        self.m2 = m2

    def __add__(self, other):
        m1 = self.m1 + other.m1
        m2 = self.m2 + other.m2
        s3 = Student(m1, m2)
        return s3

    def __gt__(self, other):
        s1 = self.m1 + self.m2
        s2 = other.m1 + other.m2
        if s1 > s2:
            return True
        else:
            return False

    def __str__(self):
        return '{} {}'.format(self.m1, self.m2)


s1 = Student(10, 20)
s2 = Student(50, 100)
print(s1.__str__())  # behind +-> it will call __str__(s1) function
print(s1)
s3 = s1 + s2  # behind +-> it will call __add__(s1,s2) function
print(s3.m1)
print(s3.m2)

if s1 > s2:  # behind +-> it will call __gt__(s1,s2) function
    print('s1 wins')
else:
    print('s2 wins')

# -----------------------------------------------------------------------------------
# METHOD OVERLOADING

# method overloading not supoorted in python, so thetrick is here :)


class Student:
    def __init__(self, m1, m2):
        self.m1 = m1
        self.m2 = m2

    def diff(self, a=None, b=None, c=None):
        diff = 0
        if a != None and b != None and c != None:
            diff = a - b - c
        elif a != None and b != None:
            diff = a - b
        else:
            diff = a

        return diff


s1 = Student(10, 29)
diff = s1.diff(20, 17, 20)
s2 = Student(10, 29)
diff2 = s2.diff(20, 17)
print(diff)
print(diff2)

# -------------------------

# ** METHOD OVERIDDING **


class A():
    def show(self):
        print("in A Show")


class B(A):
    def show(self):
        print("in B Show")


b1 = B()
b1.show()
