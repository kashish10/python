from typing import Iterator


nums = [7, 8, 9, 5]

# for i in nums:
#     print(i)

it = iter(nums)
print(it.__next__())  # 7
print(next(it))  # 5

for i in range(len(nums)):
    print(nums[i])
print("-----------------------------------------")


class TopTen():
    def __init__(self):
        self.num = 1

    def __iter__(self):  # Iter gives the object of iterator
        return self

    def __next__(self):  # next gives the next object of iterator
        if self.num <= 10:
            val = self.num
            self.num += 1
            return val
        else:
            raise StopIteration


values = TopTen()
print(next(values))
print()
for i in values:
    print(i)
