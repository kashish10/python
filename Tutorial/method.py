"""

Understanding Methods
Types of methods :
1. static method - if you dont wanna do anything with class variables or instance variables.
2. class method - Uses class variables, and object is - cls
3. Instance Method  : Uses instance variable, self. Accessor methods (Only fetching the value) and mutators methods (updating/modifying the values)

"""


class Student:

    school = 'Delhi Public School, SushantLok'  # Class Variable

    def __init__(self, m1, m2, m3):
        self.m1 = m1
        self.m2 = m2
        self.m3 = m3

    def avg(self):  # Instance Method
        average = (self.m1 + self.m2 + self.m3)/3
        return average

    def get_m1(self):  # acessor method - getter
        return self.m1

    def set_m1(self, value):  # mutator method - setter
        self.m1 = value

    @classmethod  # decorator - used when class method is there.
    def schoolName(cls):  # class method - u have to use 'cls'
        return cls.school

    @staticmethod  # decorator - used when static method is there.
    def info():
        print('This is a student class, explaining about different types of methods. And I am Static Method')


s1 = Student(56, 78, 66)
s2 = Student(70, 30, 90)

# average is an instance method as it works with an object #Instance Method
average = s1.avg()
print('Average result of S1 : ', average)
print(s1.school)
print(Student.schoolName())
Student.info()
