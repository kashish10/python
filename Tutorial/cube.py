# to print cube of numbers from user as well as command line:
import sys
from math import pow

a = int(input('Enter the number: '))
print(pow(a, 3))

x = int(sys.argv[1])
print(pow(x, 3))
