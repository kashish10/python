
from threading import *
from time import sleep


class Hello(Thread):
    def run(self):
        for i in range(0, 5):
            print('Hello')
            sleep(1)


class Hi(Thread):
    def run(self):
        for i in range(0, 5):
            print('Hi')
            sleep(1)


h1 = Hello()
h2 = Hi()

# this execution is done by main thread
h1.run()
h2.run()

# creating multi threading
print("---- Threading ----")
t1 = Hello()
t2 = Hi()
t1.start()
sleep(0.2)
t2.start()

# t1.run()
# t2.run()
t1.join()  # Wait until the thread terminates.
t2.join()
print('BYE')
