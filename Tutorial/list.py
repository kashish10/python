nums = ['Kashish', 'Chaurasia']
# print(nums)

num1 = [1, 2, 3, 4, 5]
print(num1[2:])  # o/p -> [3, 4, 5]
print(num1[-1])  # 5

combine = [nums, num1]
# list of lists printed. >> [['Kashish', 'Chaurasia'], [1, 2, 3, 4, 5]]
print(combine)

#List is mulatble
num1.append(100)
num1.insert(1, 10)
print(num1)  # [1, 10, 2, 3, 4, 5, 100]
num1.remove(100)  # use elemt to remove
num1.pop(0)  # use index to remove
num1.pop()  # removes the last elemnt of the list
del nums[2:]  # deletes values from index 2
num1.extend([2, 3, 4, 5, 6])  # add multiple values in list
min(num1)
max(num1)
print(num1)  # [10, 2, 3, 4, 2, 3, 4, 5, 6]
num1.sort()
print(num1)  # [2, 2, 3, 3, 4, 4, 5, 6, 10]

variable = "Telusko"
print(variable[-3])  # s
