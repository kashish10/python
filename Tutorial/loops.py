# print :
# kashish Chaurasia chaurasia chaurasia Chaurasia
# kashish Chaurasia chaurasia chaurasia Chaurasia
# kashish Chaurasia chaurasia chaurasia Chaurasia
# kashish Chaurasia chaurasia chaurasia Chaurasia
# kashish Chaurasia chaurasia chaurasia Chaurasia
# kashish Chaurasia chaurasia chaurasia Chaurasia
# kashish Chaurasia chaurasia chaurasia Chaurasia
# kashish Chaurasia chaurasia chaurasia Chaurasia

# 8 Times Kashish and 4 times Chaurasia

i = 1

while i <= 8:
    print('Kashish ', end='')
    j = 1
    while j <= 4:
        print('Chaurasia ', end='')
        j = j+1
    i = i+1
    print()

# print all the numbers from 1-100. Skip which are divisble by 3 and 5

i = 1
while i <= 100:
    if ((i % 3 == 0) or (i % 5 == 0)):
        i = i+1
    else:
        print(i)
        i = i+1

# Print :

    # # # # #
    # # # # #
    # # # # #
    # # # # #

i = 1
while i <= 4:
    print('# ', end="")
    j = 1
    while j <= 4:
        print('# ', end='')
        j = j+1
    i = i+1
    print()

# Print all the perfect square numbers 1- 50

for i in range(1, 51):
    # print(i*i)
    ps = i*i
    if ps in range(1, 51):
        print(ps)

# print candies depending upont the user input. Stock = 10

stock = 10

# demand = int(input('Hello user ! How many candies you want?'))
demand = 29
i = 1
while i <= demand:
    if(i == 11):
        print('Sorry, Candies are Out Of Stock ! We will be back soon :(')
        break
    print('Candy')
    i += 1


# Dont print odd numbers

for x in range(1, 101):
    if x % 2 != 0:
        continue
    else:
        print(x)

# Print :

    #
    #   #
    #   #   #
    #   #   #   #

for i in range(1, 5):
    for j in range(1, i+1):
        print('# ', end='')
    print()
print()

# Print:

#   #   #   #
#   #   #
#   #
#

for i in range(0, 4):
    for j in range(4, i, -1):
        print('# ', end='')
    print()
print()

# Print :

# 1   2   3   4
# 2   3   4
# 3   4
# 4

for i in range(5):
    for j in range(i+1, 5):
        print(j, " ", end='')
    print()
print()
# Print :
# A   P   Q   R
# A   B   Q   R
# A   B   C   R
# A   B   C   D

array1 = 'ABCD'
array2 = 'PQR'

for i in range(0, 4):
    print(array1[:i+1], end='')
    for j in range(0, i+1):
       print(array2[i:])
       break
    print()
