# Understanding Variables :


class Car:
    wheels = 4  # class variable/static variable, stored in class namespace

    def __init__(self):
        self.mil = 10  # instance, it can be changed, stored in Instance namesapce
        self.com = 'BMW'  # instance variable, it can be changed, stored in Class Namespace


Car.wheels = 2
car1 = Car()
print(car1.mil, car1.com, car1.wheels)
car2 = Car()
car2.com = 'Mercedes'
print(car2.mil, car2.com, car2.wheels)

print("-------------------------------------------------------------")
