"""
In Iterator, we have to override 2 functions , __next__(), and __iterator__(). 
But in genrator, one function.
If we use yield, its a generator!

When to use : If we are accessing dB, there is no need of fetching the whole data. We can use generator and fetch it one by one.
"""


def TopTen():
    i = 1
    while(i <= 10):
        yield i*i
        i += 1


values = TopTen()
print(values)
print(values.__next__())
print(values.__next__())
# print(values.__next__())

for i in values:
    print(i)
