# -------------------------------------------------BUBBLE SORT----------------------------------------------------------
"""
Best Case : Sorted array as input. Or almost all elements are in proper place. [ O(N) ]. O(1) swaps.
Worst Case: Reversely sorted / Very few elements are in proper place. [ O(N^2) ] . O(N^2) swaps.
Average Case: [ O(N^2) ] . O(N^2) swaps.
Space Complexity: A temporary variable is used in swapping [ auxiliary, O(1) ]. Hence it is In-Place sort. 

Slower, Comparetively

"""
from typing import List


list = [5, 3, 8, 6, 7, 2]

iterations = 0


def bubbleSort(list):
    # For Iterations. Last element will be Largest. So the array size will keep on reducing by 1
    for i in range(len(list)-1, 0, -1):
        globals()['iterations'] = globals()['iterations'] + 1
        for j in range(i):  # For Swapping
            if (list[j] > list[j+1]):
                temp = list[j+1]
                list[j+1] = list[j]
                list[j] = temp
            else:
                pass
    return list


print(list)
bubbleSort(list)
print("Bubble Sorted List : ", list, "    After :", iterations, " iterations.")

# -------------------------------------------------SELECTION SORT----------------------------------------------------------
"""
Swapping consumes a lot of CPU power and Memory .

"""
list = [5, 3, 8, 6, 7, 2]


def selectionSort(list):
    # max= highest index to lowest index, min = lowest index to highest index
    for i in range(0, len(list)-1):
        minpos = i
        for j in range(i, len(list)):
            if list[j] < list[minpos]:
                minpos = j
        temp = list[i]
        list[i] = list[minpos]
        list[minpos] = temp
        print(i, "-----", list)
    return list


print("Original list :  ", list)
selectionSort(list)
print("Selection Sorted List : ", list)
